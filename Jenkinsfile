#!/usr/bin/env groovy

node {

    stage('Checkout') {
        def scmVars = checkout scm

        def packageJSON = readJSON file: 'package.json'
        def packageJSONVersion = packageJSON.version
        
        if (packageJSONVersion) {
            echo "Build version ${packageJSONVersion} and branch name ${scmVars.GIT_BRANCH}"
        }
    }

    stage("Install") {
        withEnv(["PATH+NODE=${tool 'node'}/bin"]) {
            sh "npm install"
        }    
    }
    
    stage("Build") {
        withEnv(["PATH+NODE=${tool 'node'}/bin"]) {
            sh "npm run build-ci"
            archiveArtifacts artifacts: 'dist/*'
        }    
    }

    stage("Test") {
        withEnv(["PATH+NODE=${tool 'node'}/bin"]) {
            if(env.WITH_COVERAGE == "true") {
                sh "npm run test-ci"
                archiveArtifacts artifacts: 'coverage/**/*'
            } else {
                sh "npm run test-ci-without-coverage"
            }
        }
    }
    
    stage('SonarQube') {
        withEnv(["PATH+NODE=${tool 'node'}/bin"]) {
            sh "/var/jenkins_home/.sonar/bin/sonar-scanner -Dsonar.organization=jscriber -Dsonar.projectKey=JScriber_examen15052020 -Dsonar.sources=. -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=${SONAR_LOGIN}"
        }
    }
}
